import store from '../main.store.js'
import socket from '../../helpers/websocket'

socket.on('transport-state', (player) => {
    console.log('transport-state', player);
    store.dispatch({ type: 'PLAYER_UPDATE', playerId: player.roomName, player })
    playerRemote.fetchQueue(player.roomName)
});

socket.on('volume-change', (event) => {
    console.log('volume-change', event);
    store.dispatch({ type: 'PLAYER_VOLUME_UPDATE', playerId: event.roomName, event })
});

socket.on('topology-change', (topology) => console.log('topology-change', topology));

const playerRemote = {
    setVolume: (playerId, value) => {
      store.dispatch(dispatch => {
          return fetch(`${document.baseURI}sonos/player/${playerId}/volume/${value}`)
              .then(response => response.text())
              .then(doc => dispatch({ type: 'PLAYER_REQUESTED_VOLUME', playerId, value }))
      })
    },

    play: (playerId) => {
      store.dispatch(dispatch => {
          return fetch(`${document.baseURI}sonos/player/${playerId}/play`)
              .then(response => response.text())
              .then(doc => dispatch({ type: 'PLAYER_REQUESTED_PLAY', playerId }))
      })
    },

    pause: (playerId) => {
      store.dispatch(dispatch => {
          return fetch(`${document.baseURI}sonos/player/${playerId}/pause`)
              .then(response => response.text())
              .then(doc => dispatch({ type: 'PLAYER_REQUESTED_PAUSE', playerId }))
      })
    },

    seekTrack: (playerId, trackNo) => {
      store.dispatch(dispatch => {
          return fetch(`${document.baseURI}sonos/player/${playerId}/track/seek/${trackNo}`)
      })
    },

    nextTrack: (playerId, trackNo) => {
      store.dispatch(dispatch => {
          return fetch(`${document.baseURI}sonos/player/${playerId}/track/next`)
      })
    },

    previousTrack: (playerId, trackNo) => {
      store.dispatch(dispatch => {
          return fetch(`${document.baseURI}sonos/player/${playerId}/track/previous`)
      })
    },

    fetchQueue: (playerId) => {
      store.dispatch(dispatch => {
          return fetch (`${document.baseURI}sonos/player/${playerId}/queue`)
              .then(response => response.json())
              .then(queue => dispatch({ type: 'PLAYER_QUEUE_UPDATE', playerId, queue }))
      })
    },

    fetchState: (playerId) => {
      store.dispatch(dispatch => {
          const updatePlayer = fetch(`${document.baseURI}sonos/player/${playerId}`)
              .then(response => response.json())
              .then(doc => dispatch({ type: 'PLAYER_UPDATE', playerId: doc.roomName, player: doc }))
          const updateState = playerRemote.fetchQueue(playerId)
          return Promise.all([updatePlayer, updateState])
      })
    },



}

export default playerRemote
