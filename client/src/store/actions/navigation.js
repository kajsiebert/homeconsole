import store from '../main.store.js'

export function setPath(path) {
  console.log("SET PATH", path)
  store.dispatch({ 
    type: 'SET_PATH',
    path
  })
}
