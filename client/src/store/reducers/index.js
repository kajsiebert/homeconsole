import { combineReducers } from 'redux'
import navigation from './navigation.js'
import player from './player.js'

const rootReducer = combineReducers({
 navigation, player
})

export default rootReducer