const initialState = {activePlayerId: 'Living Room'}

const initPlayer = (state, playerId) => {
  if(!state[playerId]) state[playerId] = {}
}


export default function player(state = initialState, action) {
  switch (action.type) {
    case 'PLAYER_SWITCH_ACTIVE':
      state.activePlayerId = action.playerId;
      return state

    case 'PLAYER_UPDATE':
      console.log(`PLAYER ${action.playerId}`, action.player)
      initPlayer(state, action.playerId)
      state[action.playerId].player = action.player
      return state

    case 'PLAYER_VOLUME_UPDATE':
      console.log(`PLAYER ${action.playerId}`, action.event)
      initPlayer(state, action.playerId)
      if (state[action.playerId].player) {
      	state[action.playerId].player.state.volume = action.event.newVolume;
      }
      return state

    case 'PLAYER_QUEUE_UPDATE':
      console.log(`QUEUE ${action.playerId}`, action.queue)
      initPlayer(state, action.playerId)
      state[action.playerId].queue = action.queue;
      action.queue.items.forEach((item, index) => {
        item.trackNo = index + 1;
      });

      return state
      
    default:
      return state
  }
}