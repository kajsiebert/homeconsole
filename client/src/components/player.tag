import player from '../store/actions/player'

const playerID = 'Kitchen'

<player>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">

        <div class="container-fluid">



        <p>Player is currently: { getPlayer(opts).state.playbackState }</p>
        <p>Volume is: { getPlayer(opts).state.volume }</p>

        <button onClick={play()}>
          <i class="fa fa-play" aria-hidden="true"></i><br />
          Play
        </button>

        <button onClick={pause()}>
          <i class="fa fa-pause" aria-hidden="true"></i><br />
          Pause
        </button>

      </div>
    </div>
  </div>

  <script>
    this.play = () => () => player.play(playerID)
    this.pause = () => () => player.pause(playerID)

    this.getPlayer = opts => opts.player[playerID]

    this.on('mount', function() {
      player.fetchState(playerID)
    });

  </script>

</player>