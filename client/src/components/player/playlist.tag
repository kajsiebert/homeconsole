import player from '../../store/actions/player'
import url from 'url'
<playlist>

  <div class="container-fluid">
    <div each={ getQueue().items } class="row" 
          id={ playlistCurrentTrack : trackNo == getPlayer().state.trackNo } 
          onclick={ () => seekTrack(trackNo) } >
        <div class="title">{ title }</div>
        <div class="artist">{ artist }</div>
    </div>
  </div>

  <style>
     #playlistCurrentTrack {background-color: #ccc}
     playlist .container-fluid {height: 80vh; overflow-y: scroll;}

     playlist .row {margin: 8px 0 8px 0; cursor: pointer;}
     playlist img {height: 20px; width: 20px;}

  </style>

  <script>
    this.getPlayer = () => opts.player[opts.player.activePlayerId].player
    this.getQueue = () => opts.player[opts.player.activePlayerId].queue
    this.seekTrack = (trackNo) => player.seekTrack(opts.player.activePlayerId, trackNo)

    // Must be a better way - but for now seems we have to construct absolute URL
    // from the current track URL
    this.getTrackUrl = (albumArtUri) => {
      const currentTrackUrl = url.parse(this.getPlayer().state.currentTrack.absoluteAlbumArtUri)
      const trackurl = url.parse(albumArtUri)
      trackurl.protocol = currentTrackUrl.protocol
      trackurl.host = currentTrackUrl.host
      return url.format(trackurl)
    }

    this.on('mount', function() {
    });

    this.on('updated', function() {
      // Scroll the current playlist item into view
      const playlistCurrentTrack = document.getElementById('playlistCurrentTrack')
      if (playlistCurrentTrack) {
        const trackOffset = offset(playlistCurrentTrack);
        console.log("Attempting to scroll into view")
        if (trackOffset.top < 0) {
          playlistCurrentTrack.scrollIntoView( true );
        } else if (trackOffset.top > this.root.firstChild.clientHeight) {
           playlistCurrentTrack.scrollIntoView( false );
        }
      }

    });

    function offset(element) {
        var documentElem,
          box = { top: 0, left: 0 },
          doc = element && element.ownerDocument;
      
        if (!doc) { 
          return;
        }
      
        documentElem = doc.documentElement;
      
        if ( typeof element.getBoundingClientRect !== undefined ) {
          box = element.getBoundingClientRect();
        }
        
        return {
          top: box.top + (window.pageYOffset || documentElem.scrollTop) - (documentElem.clientTop || 0),
          left: box.left + (window.pageXOffset || documentElem.scrollLeft) - (documentElem.clientLeft || 0)
        };
      }
  </script>


</playlist>