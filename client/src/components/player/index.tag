import player from '../../store/actions/player'
import './nowplaying.tag!'
import './playlist.tag!'

<player>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-6 col-md-6">
        <nowplaying player={opts.player}/>
      </div>
      <div class="col-xs-6 col-md-6">
        <playlist player={opts.player}/>
      </div>
    </div>
  </div>

  <script>
    this.on('mount', function() {
      player.fetchState(opts.player.activePlayerId)
    });
  </script>

</player>