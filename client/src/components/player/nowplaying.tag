import player from '../../store/actions/player'
<nowplaying>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-offset-2 col-md-offset-2 col-xs-8 col-md-8">

      	<img src={ getCurrentTrack().absoluteAlbumArtUri } class="img-responsive" alt="{ getCurrentTrack().title } - { getCurrentTrack().artist }">

      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-12">
      	<h4 class="title">
      		{ getCurrentTrack().title }
      	</h4>
      	<h5 class="artist">
	      	{ getCurrentTrack().artist }
      	</h5>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-1 col-md-1"></div>
      <div class="col-xs-2 col-md-2 text-center center-block">
      		<button onClick={ () => setVolume('-2') }><i class="fa fa-volume-down" aria-hidden="true"></i></button>
      </div>
      <div class="col-xs-2 col-md-2 text-center center-block">
          <button onClick={ previousTrack }><i class="fa fa-backward" aria-hidden="true"></i></button>
      </div>
      <div class="col-xs-2 col-md-2 text-center center-block">
          <button onClick={ togglePlay }><i class="fa {getPlayButtonIcon()}" aria-hidden="true"></i></button>
      </div>
      <div class="col-xs-2 col-md-2 text-center center-block">
          <button onClick={ nextTrack }><i class="fa fa-forward" aria-hidden="true"></i></button>
      </div>
      <div class="col-xs-2 col-md-2 text-center center-block">
      		<button onClick={ () => setVolume('+2') }><i class="fa fa-volume-up" aria-hidden="true"></i></button>
      </div>
    </div>


  </div>

  <style>
     nowplaying img {width: 275px; height: 275px !important;}
  	 nowplaying .title, nowplaying .artist {text-align: center; overflow: hidden}
     nowplaying button {width: 55px; height: 55px;}
     nowplaying .img-responsive { margin: 0 auto;}
  </style>

  <script>
  	this.getPlayer = () => opts.player[opts.player.activePlayerId].player
  	this.getCurrentTrack = () => this.getPlayer().state.currentTrack
    this.getPlaybackState = () => this.getPlayer().state.playbackState
    this.getPlayButtonIcon = () => {
      switch (this.getPlaybackState()) {
          case "PLAYING": 
              return "fa-pause"
          case "PAUSED_PLAYBACK": 
          case "STOPPED": 
              return "fa-play"
          default:
              return "fa-spinner fa-pulse"
      }
    }
    this.togglePlay = () => {
      switch (this.getPlaybackState()) {
          case "PLAYING": 
              return player.pause(opts.player.activePlayerId)
          default:
              return player.play(opts.player.activePlayerId)
      }
    }

  	this.setVolume = (level) => player.setVolume(opts.player.activePlayerId, level)
    this.nextTrack = () => player.nextTrack(opts.player.activePlayerId)
    this.previousTrack = () => player.previousTrack(opts.player.activePlayerId)

    this.on('mount', function() {
    });

  </script>


</nowplaying>