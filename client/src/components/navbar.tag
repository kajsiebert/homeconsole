import {setPath} from '../store/actions/navigation'

<navbar>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" onClick={setPath([''])}><span class="fa fa-home" aria-hidden="true"></span></a>
      </div>
    </div><!-- /.container-fluid -->
  </nav>

  <script>
    this.setPath = path => () => {setPath(path)}
  </script>
</navbar>