import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css!'
import 'font-awesome/css/font-awesome.css!'

import store from '../store/main.store'
import {setPath} from '../store/actions/navigation'

import './navbar.tag!'
import './welcome.tag!'
import player from './player/index.tag!'

<index>
  <navbar />

  <welcome button={this.store.button} if={path[0] === ''} />
  <player player={this.store.player} if={path[0] === 'player'} />

  <script>
    this.on('mount', function() {
      const rootPaths = document.baseURI.split('/').slice(3).length
      setPath(window.location.pathname.split('/').slice(rootPaths))
      window.onpopstate = el => setPath(el.state)
    })

    this.on('update', function() {
      if (!this.store) return;

      if (window.location.hostname === '127.0.0.1') {
        console.log(this.store)
      }

      if (this.store.navigation.path) { 
        this.path = this.store.navigation.path
        window.history.pushState(this.path, this.path.join(' - '), this.path.join('/'));
        document.title = this.path.join(' - ')
      }
    })
  </script>

</index>