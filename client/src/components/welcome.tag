import {setPath} from '../store/actions/navigation'

<welcome>

    <button onClick={setPath(['player'])}><i class="fa fa-music" aria-hidden="true"></i><br />Music</button>
    <button disabled><i class="fa fa-cloud" aria-hidden="true"></i><br />Weather</button>
    <button disabled><i class="fa fa-subway" aria-hidden="true"></i><br />Tube</button>
    <button disabled><i class="fa fa-bus" aria-hidden="true"></i><br />Busses</button>

  <style>
    welcome {padding:5px;}
    welcome button {font-size: 24px; width: 100px; height: 75px}
    welcome button {disabled}
  </style>

  <script>
    this.setPath = path => () => {setPath(path)}
 </script>
</welcome>