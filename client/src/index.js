import riot from 'riot'
import websocket from './helpers/websocket'
import store from './store/main.store'
import './components/index.tag!'
let tags

function initDocument() {
  const body = document.createElement('body')
  const index = document.createElement('index')
  body.appendChild(index)
  document.body = body
}

function mountTags() {
  tags = riot.mount('*')
  giveStateToTags(tags)
}

function refreshOnChange() {
  store.subscribe(store => giveStateToTags())  
}

function giveStateToTags() {
  tags.forEach(tag => tag.update({ store: store.getState() }))
}

initDocument()
mountTags()
refreshOnChange()

