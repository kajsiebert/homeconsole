SystemJS.config({
  paths: {
    'npm:': 'jspm_packages/npm/',
    'github:': 'jspm_packages/github/',
    'socket-bundle': 'socket.io/socket.io.js',
    'homeconsole/': 'src/'
  },
  meta: {
    'socket-bundle': {
      'build': false
    }
  },
  browserConfig: {
    'baseURL': '/'
  },
  transpiler: 'plugin-babel',
  map: {
    'babel-runtime': 'npm:babel-runtime@5.8.38',
    'css': 'github:systemjs/plugin-css@0.1.23',
    'plugin-babel': 'npm:systemjs-plugin-babel@0.0.9',
    'plugin-babel-runtime': 'npm:babel-runtime@5.8.38',
    'tag': 'npm:jspm-riot@2.0.0',
    'scss': 'github:mobilexag/plugin-sass@0.4.3',
    'source-map': 'npm:source-map@0.1.43',
    'socket.io': 'socket-bundle'
  },
  packages: {
    'homeconsole': {
      'main': 'index.js'
    },
    'github:jspm/nodelibs-url@0.1.0': {
      'map': {
        'url': 'npm:url@0.10.3'
      }
    },
    'github:mobilexag/plugin-sass@0.4.3': {
      'map': {
        'autoprefixer': 'npm:autoprefixer@6.3.6',
        'fs': 'github:jspm/nodelibs-fs@0.1.2',
        'lodash': 'npm:lodash@4.13.1',
        'path': 'github:jspm/nodelibs-path@0.1.0',
        'postcss': 'npm:postcss@5.0.21',
        'reqwest': 'github:ded/reqwest@2.0.5',
        'sass.js': 'npm:sass.js@0.9.10',
        'url': 'github:jspm/nodelibs-url@0.1.0'
      }
    },
    'npm:autoprefixer@6.3.6': {
      'map': {
        'browserslist': 'npm:browserslist@1.3.1',
        'caniuse-db': 'npm:caniuse-db@1.0.30000470',
        'normalize-range': 'npm:normalize-range@0.1.2',
        'num2fraction': 'npm:num2fraction@1.2.2',
        'postcss': 'npm:postcss@5.0.21',
        'postcss-value-parser': 'npm:postcss-value-parser@3.3.0'
      }
    },
    'npm:browserslist@1.3.1': {
      'map': {
        'caniuse-db': 'npm:caniuse-db@1.0.30000470'
      }
    },
    'npm:postcss@5.0.21': {
      'map': {
        'js-base64': 'npm:js-base64@2.1.9',
        'source-map': 'npm:source-map@0.5.6',
        'supports-color': 'npm:supports-color@3.1.2'
      }
    },
    'npm:source-map@0.1.43': {
      'map': {
        'amdefine': 'npm:amdefine@1.0.0'
      }
    },
    'npm:supports-color@3.1.2': {
      'map': {
        'has-flag': 'npm:has-flag@1.0.0'
      }
    },
    'npm:url@0.10.3': {
      'map': {
        'punycode': 'npm:punycode@1.3.2',
        'querystring': 'npm:querystring@0.2.0'
      }
    }
  }
});

SystemJS.config({
  packageConfigPaths: [
    'npm:@*/*.json',
    'npm:*.json',
    'github:*/*.json'
  ],
  map: {
    'font-awesome': 'npm:font-awesome@4.6.3',
    'redux-thunk': 'npm:redux-thunk@2.1.0',
    'jquery': 'npm:jquery@2.2.4',
    'systemjs/plugin-css': 'github:systemjs/plugin-css@0.1.23',
    'bootstrap': 'github:twbs/bootstrap@3.3.6',
    'systemjs-plugin-babel': 'npm:systemjs-plugin-babel@0.0.9',
    'node-fetch': 'npm:node-fetch@1.5.3',
    'redux': 'npm:redux@3.5.2',
    'seamless-immutable': 'npm:seamless-immutable@6.1.0',
    'jspm-riot': 'npm:jspm-riot@2.0.0',
    'assert': 'github:jspm/nodelibs-assert@0.2.0-alpha',
    'buffer': 'github:jspm/nodelibs-buffer@0.2.0-alpha',
    'child_process': 'github:jspm/nodelibs-child_process@0.2.0-alpha',
    'cluster': 'github:jspm/nodelibs-cluster@0.2.0-alpha',
    'constants': 'github:jspm/nodelibs-constants@0.2.0-alpha',
    'crypto': 'github:jspm/nodelibs-crypto@0.2.0-alpha',
    'dgram': 'github:jspm/nodelibs-dgram@0.2.0-alpha',
    'dns': 'github:jspm/nodelibs-dns@0.2.0-alpha',
    'ecc-jsbn': 'npm:ecc-jsbn@0.1.1',
    'events': 'github:jspm/nodelibs-events@0.2.0-alpha',
    'fs': 'github:jspm/nodelibs-fs@0.2.0-alpha',
    'fsevents': 'npm:fsevents@1.0.12',
    'http': 'github:jspm/nodelibs-http@0.2.0-alpha',
    'https': 'github:jspm/nodelibs-https@0.2.0-alpha',
    'jodid25519': 'npm:jodid25519@1.0.2',
    'jsbn': 'npm:jsbn@0.1.0',
    'module': 'github:jspm/nodelibs-module@0.2.0-alpha',
    'net': 'github:jspm/nodelibs-net@0.2.0-alpha',
    'os': 'github:jspm/nodelibs-os@0.2.0-alpha',
    'path': 'github:jspm/nodelibs-path@0.2.0-alpha',
    'process': 'github:jspm/nodelibs-process@0.2.0-alpha',
    'punycode': 'github:jspm/nodelibs-punycode@0.2.0-alpha',
    'riot': 'npm:riot@3.0.0-alpha.4',
    'stream': 'github:jspm/nodelibs-stream@0.2.0-alpha',
    'string_decoder': 'github:jspm/nodelibs-string_decoder@0.2.0-alpha',
    'querystring': 'github:jspm/nodelibs-querystring@0.2.0-alpha',
    'tls': 'github:jspm/nodelibs-tls@0.2.0-alpha',
    'tty': 'github:jspm/nodelibs-tty@0.2.0-alpha',
    'tweetnacl': 'npm:tweetnacl@0.13.3',
    'url': 'github:jspm/nodelibs-url@0.2.0-alpha',
    'util': 'github:jspm/nodelibs-util@0.2.0-alpha',
    'vm': 'github:jspm/nodelibs-vm@0.2.0-alpha',
    'zlib': 'github:jspm/nodelibs-zlib@0.2.0-alpha'
  },
  packages: {
    'npm:font-awesome@4.6.3': {
      'map': {
        'css': 'github:systemjs/plugin-css@0.1.23'
      }
    },
    'github:twbs/bootstrap@3.3.6': {
      'map': {
        'jquery': 'npm:jquery@2.2.4'
      }
    },
    'npm:loose-envify@1.2.0': {
      'map': {
        'js-tokens': 'npm:js-tokens@1.0.3'
      }
    },
    'npm:redux@3.5.2': {
      'map': {
        'lodash': 'npm:lodash@4.13.1',
        'lodash-es': 'npm:lodash-es@4.13.1',
        'loose-envify': 'npm:loose-envify@1.2.0',
        'symbol-observable': 'npm:symbol-observable@0.2.4'
      }
    },
    'npm:anymatch@1.3.0': {
      'map': {
        'arrify': 'npm:arrify@1.0.1',
        'micromatch': 'npm:micromatch@2.3.8'
      }
    },
    'npm:arr-diff@2.0.0': {
      'map': {
        'arr-flatten': 'npm:arr-flatten@1.0.1'
      }
    },
    'npm:brace-expansion@1.1.4': {
      'map': {
        'balanced-match': 'npm:balanced-match@0.4.1',
        'concat-map': 'npm:concat-map@0.0.1'
      }
    },
    'npm:chalk@1.1.3': {
      'map': {
        'ansi-styles': 'npm:ansi-styles@2.2.1',
        'escape-string-regexp': 'npm:escape-string-regexp@1.0.5',
        'has-ansi': 'npm:has-ansi@2.0.0',
        'strip-ansi': 'npm:strip-ansi@3.0.1',
        'supports-color': 'npm:supports-color@2.0.0'
      }
    },
    'npm:expand-brackets@0.1.5': {
      'map': {
        'is-posix-bracket': 'npm:is-posix-bracket@0.1.1'
      }
    },
    'npm:expand-range@1.8.2': {
      'map': {
        'fill-range': 'npm:fill-range@2.2.3'
      }
    },
    'npm:extglob@0.3.2': {
      'map': {
        'is-extglob': 'npm:is-extglob@1.0.0'
      }
    },
    'npm:fill-range@2.2.3': {
      'map': {
        'is-number': 'npm:is-number@2.1.0',
        'isobject': 'npm:isobject@2.1.0',
        'randomatic': 'npm:randomatic@1.1.5',
        'repeat-element': 'npm:repeat-element@1.1.2',
        'repeat-string': 'npm:repeat-string@1.5.4'
      }
    },
    'npm:for-own@0.1.4': {
      'map': {
        'for-in': 'npm:for-in@0.1.5'
      }
    },
    'npm:glob-base@0.3.0': {
      'map': {
        'glob-parent': 'npm:glob-parent@2.0.0',
        'is-glob': 'npm:is-glob@2.0.1'
      }
    },
    'npm:glob-parent@2.0.0': {
      'map': {
        'is-glob': 'npm:is-glob@2.0.1'
      }
    },
    'npm:glob@7.0.3': {
      'map': {
        'inflight': 'npm:inflight@1.0.5',
        'inherits': 'npm:inherits@2.0.1',
        'minimatch': 'npm:minimatch@3.0.0',
        'once': 'npm:once@1.3.3',
        'path-is-absolute': 'npm:path-is-absolute@1.0.0'
      }
    },
    'npm:has-ansi@2.0.0': {
      'map': {
        'ansi-regex': 'npm:ansi-regex@2.0.0'
      }
    },
    'npm:inflight@1.0.5': {
      'map': {
        'once': 'npm:once@1.3.3',
        'wrappy': 'npm:wrappy@1.0.2'
      }
    },
    'npm:is-binary-path@1.0.1': {
      'map': {
        'binary-extensions': 'npm:binary-extensions@1.4.1'
      }
    },
    'npm:is-equal-shallow@0.1.3': {
      'map': {
        'is-primitive': 'npm:is-primitive@2.0.0'
      }
    },
    'npm:is-glob@2.0.1': {
      'map': {
        'is-extglob': 'npm:is-extglob@1.0.0'
      }
    },
    'npm:is-number@2.1.0': {
      'map': {
        'kind-of': 'npm:kind-of@3.0.3'
      }
    },
    'npm:isobject@2.1.0': {
      'map': {
        'isarray': 'npm:isarray@1.0.0'
      }
    },
    'npm:kind-of@3.0.3': {
      'map': {
        'is-buffer': 'npm:is-buffer@1.1.3'
      }
    },
    'npm:levn@0.3.0': {
      'map': {
        'prelude-ls': 'npm:prelude-ls@1.1.2',
        'type-check': 'npm:type-check@0.3.2'
      }
    },
    'npm:micromatch@2.3.8': {
      'map': {
        'arr-diff': 'npm:arr-diff@2.0.0',
        'array-unique': 'npm:array-unique@0.2.1',
        'braces': 'npm:braces@1.8.5',
        'expand-brackets': 'npm:expand-brackets@0.1.5',
        'extglob': 'npm:extglob@0.3.2',
        'filename-regex': 'npm:filename-regex@2.0.0',
        'is-extglob': 'npm:is-extglob@1.0.0',
        'is-glob': 'npm:is-glob@2.0.1',
        'kind-of': 'npm:kind-of@3.0.3',
        'normalize-path': 'npm:normalize-path@2.0.1',
        'object.omit': 'npm:object.omit@2.0.0',
        'parse-glob': 'npm:parse-glob@3.0.4',
        'regex-cache': 'npm:regex-cache@0.4.3'
      }
    },
    'npm:minimatch@2.0.10': {
      'map': {
        'brace-expansion': 'npm:brace-expansion@1.1.4'
      }
    },
    'npm:minimatch@3.0.0': {
      'map': {
        'brace-expansion': 'npm:brace-expansion@1.1.4'
      }
    },
    'npm:object.omit@2.0.0': {
      'map': {
        'for-own': 'npm:for-own@0.1.4',
        'is-extendable': 'npm:is-extendable@0.1.1'
      }
    },
    'npm:once@1.3.3': {
      'map': {
        'wrappy': 'npm:wrappy@1.0.2'
      }
    },
    'npm:optionator@0.8.1': {
      'map': {
        'deep-is': 'npm:deep-is@0.1.3',
        'fast-levenshtein': 'npm:fast-levenshtein@1.1.3',
        'levn': 'npm:levn@0.3.0',
        'prelude-ls': 'npm:prelude-ls@1.1.2',
        'type-check': 'npm:type-check@0.3.2',
        'wordwrap': 'npm:wordwrap@1.0.0'
      }
    },
    'npm:parse-glob@3.0.4': {
      'map': {
        'glob-base': 'npm:glob-base@0.3.0',
        'is-dotfile': 'npm:is-dotfile@1.0.2',
        'is-extglob': 'npm:is-extglob@1.0.0',
        'is-glob': 'npm:is-glob@2.0.1'
      }
    },
    'npm:randomatic@1.1.5': {
      'map': {
        'is-number': 'npm:is-number@2.1.0',
        'kind-of': 'npm:kind-of@3.0.3'
      }
    },
    'npm:readable-stream@2.1.4': {
      'map': {
        'buffer-shims': 'npm:buffer-shims@1.0.0',
        'core-util-is': 'npm:core-util-is@1.0.2',
        'inherits': 'npm:inherits@2.0.1',
        'isarray': 'npm:isarray@1.0.0',
        'process-nextick-args': 'npm:process-nextick-args@1.0.7',
        'string_decoder': 'npm:string_decoder@0.10.31',
        'util-deprecate': 'npm:util-deprecate@1.0.2'
      }
    },
    'npm:readdirp@2.0.0': {
      'map': {
        'graceful-fs': 'npm:graceful-fs@4.1.4',
        'minimatch': 'npm:minimatch@2.0.10',
        'readable-stream': 'npm:readable-stream@2.1.4'
      }
    },
    'npm:rechoir@0.6.2': {
      'map': {
        'resolve': 'npm:resolve@1.1.7'
      }
    },
    'npm:regex-cache@0.4.3': {
      'map': {
        'is-equal-shallow': 'npm:is-equal-shallow@0.1.3',
        'is-primitive': 'npm:is-primitive@2.0.0'
      }
    },
    'npm:riot-cli@2.5.0': {
      'map': {
        'chalk': 'npm:chalk@1.1.3',
        'chokidar': 'npm:chokidar@1.5.2',
        'co': 'npm:co@4.6.0',
        'optionator': 'npm:optionator@0.8.1',
        'riot-compiler': 'npm:riot-compiler@2.4.1',
        'rollup': 'npm:rollup@0.26.7',
        'shelljs': 'npm:shelljs@0.7.0'
      }
    },
    'npm:shelljs@0.7.0': {
      'map': {
        'glob': 'npm:glob@7.0.3',
        'interpret': 'npm:interpret@1.0.1',
        'rechoir': 'npm:rechoir@0.6.2'
      }
    },
    'npm:source-map-support@0.4.0': {
      'map': {
        'source-map': 'npm:source-map@0.1.32'
      }
    },
    'npm:source-map@0.1.32': {
      'map': {
        'amdefine': 'npm:amdefine@1.0.0'
      }
    },
    'npm:strip-ansi@3.0.1': {
      'map': {
        'ansi-regex': 'npm:ansi-regex@2.0.0'
      }
    },
    'npm:type-check@0.3.2': {
      'map': {
        'prelude-ls': 'npm:prelude-ls@1.1.2'
      }
    },
    'npm:riot@3.0.0-alpha.4': {
      'map': {
        'riot-compiler': 'npm:riot-compiler@3.0.0-alpha.1',
        'riot-cli': 'npm:riot-cli@2.5.0',
        'riot-tmpl': 'npm:riot-tmpl@2.4.0',
        'simple-html-tokenizer': 'npm:simple-html-tokenizer@0.2.5',
        'riot-observable': 'npm:riot-observable@2.4.2',
        'simple-dom': 'npm:simple-dom@0.3.0'
      }
    },
    'npm:braces@1.8.5': {
      'map': {
        'preserve': 'npm:preserve@0.2.0',
        'repeat-element': 'npm:repeat-element@1.1.2',
        'expand-range': 'npm:expand-range@1.8.2'
      }
    },
    'npm:rollup@0.26.7': {
      'map': {
        'chalk': 'npm:chalk@1.1.3',
        'source-map-support': 'npm:source-map-support@0.4.0',
        'minimist': 'npm:minimist@1.2.0'
      }
    },
    'npm:chokidar@1.5.2': {
      'map': {
        'async-each': 'npm:async-each@1.0.0',
        'is-glob': 'npm:is-glob@2.0.1',
        'is-binary-path': 'npm:is-binary-path@1.0.1',
        'path-is-absolute': 'npm:path-is-absolute@1.0.0',
        'inherits': 'npm:inherits@2.0.1',
        'readdirp': 'npm:readdirp@2.0.0',
        'anymatch': 'npm:anymatch@1.3.0',
        'glob-parent': 'npm:glob-parent@2.0.0'
      }
    },
    'npm:fsevents@1.0.12': {
      'map': {
        'nan': 'npm:nan@2.3.5',
        'node-pre-gyp': 'npm:node-pre-gyp@0.6.28'
      }
    },
    'npm:node-pre-gyp@0.6.28': {
      'map': {
        'tar': 'npm:tar@2.2.1',
        'npmlog': 'npm:npmlog@2.0.4',
        'rimraf': 'npm:rimraf@2.5.2',
        'mkdirp': 'npm:mkdirp@0.5.1',
        'nopt': 'npm:nopt@3.0.6',
        'rc': 'npm:rc@1.1.6',
        'semver': 'npm:semver@5.1.0',
        'tar-pack': 'npm:tar-pack@3.1.3',
        'request': 'npm:request@2.72.0'
      }
    },
    'github:jspm/nodelibs-buffer@0.2.0-alpha': {
      'map': {
        'buffer-browserify': 'npm:buffer@4.6.0'
      }
    },
    'npm:tar@2.2.1': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'block-stream': 'npm:block-stream@0.0.9',
        'fstream': 'npm:fstream@1.0.9'
      }
    },
    'npm:rimraf@2.5.2': {
      'map': {
        'glob': 'npm:glob@7.0.3'
      }
    },
    'npm:mkdirp@0.5.1': {
      'map': {
        'minimist': 'npm:minimist@0.0.8'
      }
    },
    'github:jspm/nodelibs-os@0.2.0-alpha': {
      'map': {
        'os-browserify': 'npm:os-browserify@0.2.1'
      }
    },
    'npm:npmlog@2.0.4': {
      'map': {
        'ansi': 'npm:ansi@0.3.1',
        'are-we-there-yet': 'npm:are-we-there-yet@1.1.2',
        'gauge': 'npm:gauge@1.2.7'
      }
    },
    'npm:nopt@3.0.6': {
      'map': {
        'abbrev': 'npm:abbrev@1.0.7'
      }
    },
    'npm:rc@1.1.6': {
      'map': {
        'minimist': 'npm:minimist@1.2.0',
        'ini': 'npm:ini@1.3.4',
        'strip-json-comments': 'npm:strip-json-comments@1.0.4',
        'deep-extend': 'npm:deep-extend@0.4.1'
      }
    },
    'npm:buffer@4.6.0': {
      'map': {
        'isarray': 'npm:isarray@1.0.0',
        'ieee754': 'npm:ieee754@1.1.6',
        'base64-js': 'npm:base64-js@1.1.2'
      }
    },
    'npm:fstream@1.0.9': {
      'map': {
        'graceful-fs': 'npm:graceful-fs@4.1.4',
        'inherits': 'npm:inherits@2.0.1',
        'mkdirp': 'npm:mkdirp@0.5.1',
        'rimraf': 'npm:rimraf@2.5.2'
      }
    },
    'npm:block-stream@0.0.9': {
      'map': {
        'inherits': 'npm:inherits@2.0.1'
      }
    },
    'npm:tar-pack@3.1.3': {
      'map': {
        'readable-stream': 'npm:readable-stream@2.0.6',
        'fstream': 'npm:fstream@1.0.9',
        'once': 'npm:once@1.3.3',
        'rimraf': 'npm:rimraf@2.5.2',
        'tar': 'npm:tar@2.2.1',
        'debug': 'npm:debug@2.2.0',
        'fstream-ignore': 'npm:fstream-ignore@1.0.5',
        'uid-number': 'npm:uid-number@0.0.6'
      }
    },
    'npm:are-we-there-yet@1.1.2': {
      'map': {
        'readable-stream': 'npm:readable-stream@1.1.14',
        'delegates': 'npm:delegates@1.0.0'
      }
    },
    'npm:gauge@1.2.7': {
      'map': {
        'ansi': 'npm:ansi@0.3.1',
        'has-unicode': 'npm:has-unicode@2.0.0',
        'lodash.padstart': 'npm:lodash.padstart@4.5.0',
        'lodash.padend': 'npm:lodash.padend@4.5.0',
        'lodash.pad': 'npm:lodash.pad@4.4.0'
      }
    },
    'npm:readable-stream@2.0.6': {
      'map': {
        'core-util-is': 'npm:core-util-is@1.0.2',
        'inherits': 'npm:inherits@2.0.1',
        'isarray': 'npm:isarray@1.0.0',
        'process-nextick-args': 'npm:process-nextick-args@1.0.7',
        'string_decoder': 'npm:string_decoder@0.10.31',
        'util-deprecate': 'npm:util-deprecate@1.0.2'
      }
    },
    'npm:request@2.72.0': {
      'map': {
        'bl': 'npm:bl@1.1.2',
        'aws-sign2': 'npm:aws-sign2@0.6.0',
        'aws4': 'npm:aws4@1.4.1',
        'form-data': 'npm:form-data@1.0.0-rc4',
        'combined-stream': 'npm:combined-stream@1.0.5',
        'extend': 'npm:extend@3.0.0',
        'forever-agent': 'npm:forever-agent@0.6.1',
        'http-signature': 'npm:http-signature@1.1.1',
        'hawk': 'npm:hawk@3.1.3',
        'is-typedarray': 'npm:is-typedarray@1.0.0',
        'node-uuid': 'npm:node-uuid@1.4.7',
        'json-stringify-safe': 'npm:json-stringify-safe@5.0.1',
        'mime-types': 'npm:mime-types@2.1.11',
        'tough-cookie': 'npm:tough-cookie@2.2.2',
        'oauth-sign': 'npm:oauth-sign@0.8.2',
        'tunnel-agent': 'npm:tunnel-agent@0.4.3',
        'qs': 'npm:qs@6.1.0',
        'isstream': 'npm:isstream@0.1.2',
        'stringstream': 'npm:stringstream@0.0.5',
        'caseless': 'npm:caseless@0.11.0',
        'har-validator': 'npm:har-validator@2.0.6'
      }
    },
    'npm:fstream-ignore@1.0.5': {
      'map': {
        'fstream': 'npm:fstream@1.0.9',
        'inherits': 'npm:inherits@2.0.1',
        'minimatch': 'npm:minimatch@3.0.0'
      }
    },
    'github:jspm/nodelibs-http@0.2.0-alpha': {
      'map': {
        'http-browserify': 'npm:stream-http@2.3.0'
      }
    },
    'npm:readable-stream@1.1.14': {
      'map': {
        'isarray': 'npm:isarray@0.0.1',
        'core-util-is': 'npm:core-util-is@1.0.2',
        'inherits': 'npm:inherits@2.0.1',
        'string_decoder': 'npm:string_decoder@0.10.31',
        'stream-browserify': 'npm:stream-browserify@1.0.0'
      }
    },
    'npm:bl@1.1.2': {
      'map': {
        'readable-stream': 'npm:readable-stream@2.0.6'
      }
    },
    'npm:form-data@1.0.0-rc4': {
      'map': {
        'combined-stream': 'npm:combined-stream@1.0.5',
        'mime-types': 'npm:mime-types@2.1.11',
        'async': 'npm:async@1.5.2'
      }
    },
    'npm:stream-http@2.3.0': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'readable-stream': 'npm:readable-stream@2.1.4',
        'xtend': 'npm:xtend@4.0.1',
        'builtin-status-codes': 'npm:builtin-status-codes@2.0.0',
        'to-arraybuffer': 'npm:to-arraybuffer@1.0.1'
      }
    },
    'npm:debug@2.2.0': {
      'map': {
        'ms': 'npm:ms@0.7.1'
      }
    },
    'npm:http-signature@1.1.1': {
      'map': {
        'assert-plus': 'npm:assert-plus@0.2.0',
        'jsprim': 'npm:jsprim@1.2.2',
        'sshpk': 'npm:sshpk@1.8.3'
      }
    },
    'npm:hawk@3.1.3': {
      'map': {
        'sntp': 'npm:sntp@1.0.9',
        'cryptiles': 'npm:cryptiles@2.0.5',
        'boom': 'npm:boom@2.10.1',
        'hoek': 'npm:hoek@2.16.3'
      }
    },
    'npm:har-validator@2.0.6': {
      'map': {
        'chalk': 'npm:chalk@1.1.3',
        'commander': 'npm:commander@2.9.0',
        'is-my-json-valid': 'npm:is-my-json-valid@2.13.1',
        'pinkie-promise': 'npm:pinkie-promise@2.0.1'
      }
    },
    'github:jspm/nodelibs-stream@0.2.0-alpha': {
      'map': {
        'stream-browserify': 'npm:stream-browserify@2.0.1'
      }
    },
    'npm:combined-stream@1.0.5': {
      'map': {
        'delayed-stream': 'npm:delayed-stream@1.0.0'
      }
    },
    'github:jspm/nodelibs-zlib@0.2.0-alpha': {
      'map': {
        'zlib-browserify': 'npm:browserify-zlib@0.1.4'
      }
    },
    'npm:lodash.padend@4.5.0': {
      'map': {
        'lodash._baseslice': 'npm:lodash._baseslice@4.0.0',
        'lodash.tostring': 'npm:lodash.tostring@4.1.3',
        'lodash._basetostring': 'npm:lodash._basetostring@4.12.0'
      }
    },
    'npm:lodash.padstart@4.5.0': {
      'map': {
        'lodash._baseslice': 'npm:lodash._baseslice@4.0.0',
        'lodash.tostring': 'npm:lodash.tostring@4.1.3',
        'lodash._basetostring': 'npm:lodash._basetostring@4.12.0'
      }
    },
    'npm:lodash.pad@4.4.0': {
      'map': {
        'lodash._baseslice': 'npm:lodash._baseslice@4.0.0',
        'lodash.tostring': 'npm:lodash.tostring@4.1.3',
        'lodash._basetostring': 'npm:lodash._basetostring@4.12.0'
      }
    },
    'npm:mime-types@2.1.11': {
      'map': {
        'mime-db': 'npm:mime-db@1.23.0'
      }
    },
    'npm:cryptiles@2.0.5': {
      'map': {
        'boom': 'npm:boom@2.10.1'
      }
    },
    'npm:sntp@1.0.9': {
      'map': {
        'hoek': 'npm:hoek@2.16.3'
      }
    },
    'npm:boom@2.10.1': {
      'map': {
        'hoek': 'npm:hoek@2.16.3'
      }
    },
    'npm:stream-browserify@2.0.1': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'readable-stream': 'npm:readable-stream@2.1.4'
      }
    },
    'npm:stream-browserify@1.0.0': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'readable-stream': 'npm:readable-stream@1.1.14'
      }
    },
    'npm:is-my-json-valid@2.13.1': {
      'map': {
        'xtend': 'npm:xtend@4.0.1',
        'jsonpointer': 'npm:jsonpointer@2.0.0',
        'generate-object-property': 'npm:generate-object-property@1.2.0',
        'generate-function': 'npm:generate-function@2.0.0'
      }
    },
    'github:jspm/nodelibs-url@0.2.0-alpha': {
      'map': {
        'url-browserify': 'npm:url@0.11.0'
      }
    },
    'npm:browserify-zlib@0.1.4': {
      'map': {
        'readable-stream': 'npm:readable-stream@2.1.4',
        'pako': 'npm:pako@0.2.8'
      }
    },
    'npm:sshpk@1.8.3': {
      'map': {
        'assert-plus': 'npm:assert-plus@1.0.0',
        'asn1': 'npm:asn1@0.2.3',
        'getpass': 'npm:getpass@0.1.6',
        'dashdash': 'npm:dashdash@1.14.0'
      }
    },
    'npm:commander@2.9.0': {
      'map': {
        'graceful-readlink': 'npm:graceful-readlink@1.0.1'
      }
    },
    'npm:pinkie-promise@2.0.1': {
      'map': {
        'pinkie': 'npm:pinkie@2.0.4'
      }
    },
    'npm:jsprim@1.2.2': {
      'map': {
        'json-schema': 'npm:json-schema@0.2.2',
        'extsprintf': 'npm:extsprintf@1.0.2',
        'verror': 'npm:verror@1.3.6'
      }
    },
    'npm:getpass@0.1.6': {
      'map': {
        'assert-plus': 'npm:assert-plus@1.0.0'
      }
    },
    'npm:ecc-jsbn@0.1.1': {
      'map': {
        'jsbn': 'npm:jsbn@0.1.0'
      }
    },
    'npm:jodid25519@1.0.2': {
      'map': {
        'jsbn': 'npm:jsbn@0.1.0'
      }
    },
    'npm:verror@1.3.6': {
      'map': {
        'extsprintf': 'npm:extsprintf@1.0.2'
      }
    },
    'github:jspm/nodelibs-crypto@0.2.0-alpha': {
      'map': {
        'crypto-browserify': 'npm:crypto-browserify@3.11.0'
      }
    },
    'npm:url@0.11.0': {
      'map': {
        'querystring': 'npm:querystring@0.2.0',
        'punycode': 'npm:punycode@1.3.2'
      }
    },
    'npm:dashdash@1.14.0': {
      'map': {
        'assert-plus': 'npm:assert-plus@1.0.0'
      }
    },
    'npm:generate-object-property@1.2.0': {
      'map': {
        'is-property': 'npm:is-property@1.0.2'
      }
    },
    'github:jspm/nodelibs-punycode@0.2.0-alpha': {
      'map': {
        'punycode-browserify': 'npm:punycode@1.4.1'
      }
    },
    'npm:crypto-browserify@3.11.0': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'create-ecdh': 'npm:create-ecdh@4.0.0',
        'create-hash': 'npm:create-hash@1.1.2',
        'browserify-sign': 'npm:browserify-sign@4.0.0',
        'pbkdf2': 'npm:pbkdf2@3.0.4',
        'browserify-cipher': 'npm:browserify-cipher@1.0.0',
        'create-hmac': 'npm:create-hmac@1.1.4',
        'diffie-hellman': 'npm:diffie-hellman@5.0.2',
        'randombytes': 'npm:randombytes@2.0.3',
        'public-encrypt': 'npm:public-encrypt@4.0.0'
      }
    },
    'github:jspm/nodelibs-string_decoder@0.2.0-alpha': {
      'map': {
        'string_decoder-browserify': 'npm:string_decoder@0.10.31'
      }
    },
    'npm:create-hash@1.1.2': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'cipher-base': 'npm:cipher-base@1.0.2',
        'ripemd160': 'npm:ripemd160@1.0.1',
        'sha.js': 'npm:sha.js@2.4.5'
      }
    },
    'npm:browserify-sign@4.0.0': {
      'map': {
        'create-hash': 'npm:create-hash@1.1.2',
        'inherits': 'npm:inherits@2.0.1',
        'create-hmac': 'npm:create-hmac@1.1.4',
        'browserify-rsa': 'npm:browserify-rsa@4.0.1',
        'parse-asn1': 'npm:parse-asn1@5.0.0',
        'bn.js': 'npm:bn.js@4.11.4',
        'elliptic': 'npm:elliptic@6.2.8'
      }
    },
    'npm:create-hmac@1.1.4': {
      'map': {
        'create-hash': 'npm:create-hash@1.1.2',
        'inherits': 'npm:inherits@2.0.1'
      }
    },
    'npm:public-encrypt@4.0.0': {
      'map': {
        'create-hash': 'npm:create-hash@1.1.2',
        'randombytes': 'npm:randombytes@2.0.3',
        'browserify-rsa': 'npm:browserify-rsa@4.0.1',
        'parse-asn1': 'npm:parse-asn1@5.0.0',
        'bn.js': 'npm:bn.js@4.11.4'
      }
    },
    'npm:pbkdf2@3.0.4': {
      'map': {
        'create-hmac': 'npm:create-hmac@1.1.4'
      }
    },
    'npm:diffie-hellman@5.0.2': {
      'map': {
        'randombytes': 'npm:randombytes@2.0.3',
        'bn.js': 'npm:bn.js@4.11.4',
        'miller-rabin': 'npm:miller-rabin@4.0.0'
      }
    },
    'npm:create-ecdh@4.0.0': {
      'map': {
        'bn.js': 'npm:bn.js@4.11.4',
        'elliptic': 'npm:elliptic@6.2.8'
      }
    },
    'npm:browserify-cipher@1.0.0': {
      'map': {
        'browserify-des': 'npm:browserify-des@1.0.0',
        'browserify-aes': 'npm:browserify-aes@1.0.6',
        'evp_bytestokey': 'npm:evp_bytestokey@1.0.0'
      }
    },
    'npm:cipher-base@1.0.2': {
      'map': {
        'inherits': 'npm:inherits@2.0.1'
      }
    },
    'npm:browserify-rsa@4.0.1': {
      'map': {
        'randombytes': 'npm:randombytes@2.0.3',
        'bn.js': 'npm:bn.js@4.11.4'
      }
    },
    'npm:parse-asn1@5.0.0': {
      'map': {
        'create-hash': 'npm:create-hash@1.1.2',
        'pbkdf2': 'npm:pbkdf2@3.0.4',
        'browserify-aes': 'npm:browserify-aes@1.0.6',
        'evp_bytestokey': 'npm:evp_bytestokey@1.0.0',
        'asn1.js': 'npm:asn1.js@4.6.2'
      }
    },
    'npm:browserify-des@1.0.0': {
      'map': {
        'cipher-base': 'npm:cipher-base@1.0.2',
        'inherits': 'npm:inherits@2.0.1',
        'des.js': 'npm:des.js@1.0.0'
      }
    },
    'npm:browserify-aes@1.0.6': {
      'map': {
        'cipher-base': 'npm:cipher-base@1.0.2',
        'create-hash': 'npm:create-hash@1.1.2',
        'inherits': 'npm:inherits@2.0.1',
        'evp_bytestokey': 'npm:evp_bytestokey@1.0.0',
        'buffer-xor': 'npm:buffer-xor@1.0.3'
      }
    },
    'npm:elliptic@6.2.8': {
      'map': {
        'bn.js': 'npm:bn.js@4.11.4',
        'inherits': 'npm:inherits@2.0.1',
        'brorand': 'npm:brorand@1.0.5',
        'hash.js': 'npm:hash.js@1.0.3'
      }
    },
    'npm:evp_bytestokey@1.0.0': {
      'map': {
        'create-hash': 'npm:create-hash@1.1.2'
      }
    },
    'npm:miller-rabin@4.0.0': {
      'map': {
        'bn.js': 'npm:bn.js@4.11.4',
        'brorand': 'npm:brorand@1.0.5'
      }
    },
    'npm:sha.js@2.4.5': {
      'map': {
        'inherits': 'npm:inherits@2.0.1'
      }
    },
    'npm:asn1.js@4.6.2': {
      'map': {
        'bn.js': 'npm:bn.js@4.11.4',
        'inherits': 'npm:inherits@2.0.1',
        'minimalistic-assert': 'npm:minimalistic-assert@1.0.0'
      }
    },
    'npm:des.js@1.0.0': {
      'map': {
        'inherits': 'npm:inherits@2.0.1',
        'minimalistic-assert': 'npm:minimalistic-assert@1.0.0'
      }
    },
    'npm:hash.js@1.0.3': {
      'map': {
        'inherits': 'npm:inherits@2.0.1'
      }
    },
    'npm:jspm-riot@2.0.0': {
      'map': {
        'riot-compiler': 'npm:riot-compiler@3.0.0-alpha.1'
      }
    },
    'npm:node-fetch@1.5.3': {
      'map': {
        'is-stream': 'npm:is-stream@1.1.0',
        'encoding': 'npm:encoding@0.1.12'
      }
    },
    'npm:encoding@0.1.12': {
      'map': {
        'iconv-lite': 'npm:iconv-lite@0.4.13'
      }
    }
  }
});
