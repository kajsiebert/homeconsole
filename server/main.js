const app = require('./app');

server = new app()
server.on('server_start', (httpServer) => console.log(`server listening on port ${httpServer.address().port}`))
server.start()

