'use strict';
const express = require('express');
const fallback = require('express-history-api-fallback');
const EventEmitter = require('events').EventEmitter;
const util = require('util');
const http = require('http')
const sonosRouter = require('./lib/routes/sonos')
const path = require('path')


function HomeconsoleServer() {
}
util.inherits(HomeconsoleServer, EventEmitter);

HomeconsoleServer.prototype.start = function start(config) {
	const _this = this;
	const app = express() 
	config = config || {}

	const httpServer = http.createServer(app);
	const io = require('socket.io')(httpServer);

	io.on('connection', function(socket){
	  console.log('a user connected');
	  socket.on('disconnect', function(){
	    console.log('user disconnected');
	  });
	});

	app.use('/sonos', sonosRouter(io))

	// Serve static client files
	let staticPath = config.static || path.resolve(__dirname, `../client/`)
	if (config.bundled) {
		staticPath = path.resolve(__dirname, `dist`)
	}
	console.log('Static Path:', staticPath)
	app.use(express.static( staticPath ));
	app.use(fallback('index.html', { root: staticPath }))

	httpServer.listen(8080, () => {
	  _this.emit('server_start', httpServer);
	})		
}

module.exports = HomeconsoleServer