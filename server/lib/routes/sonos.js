const SonosDiscovery = require('sonos-discovery')
const express = require('express');


module.exports = (io) => {

  var discovery = new SonosDiscovery({});
  discovery.on('transport-state', function (player) {
  	console.log('transport-state');
    io.emit('transport-state', player);
  });

  discovery.on('topology-change', function (topology) {
  	console.log('topology-change');
    io.emit('topology-change', topology);
  });

  discovery.on('volume-change', (event) => {
    console.log('volume-change', event);
    io.emit('volume-change', event);
  });

  const router = express.Router();

  router.get('/players', function(req, res) {
    res.send(discovery.players);
  });

  // FV:0 - Radio favourites
  // FV:2 - Playlists
  // 
  router.get('/browse/:objectId', function(req, res) {
    discovery.getAnyPlayer().browse(req.params.objectId)
      .then(result => res.send(result))
  });

  router.get('/player/:playerId/playlist/:objectId', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.clearQueue()
      .then((result) => console.log('Queue cleared', result))
      .then(() => {return player.addURIToQueue(`file:///jffs/settings/savedqueues.rsq#${req.params.objectId}`)})
      .then(console.log('Playlist scheduled'))
      .then(res.send("OK"))
  });

  router.get('/player/:playerId/queue', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.browse('Q:0')
      .then((result) => {res.send(result)})
  });

  router.get('/player/:playerId', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    return res.send(player)
  });

  router.get('/player/:playerId/play', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.play()
    return res.send("OK")
  });

  router.get('/player/:playerId/pause', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.pause()
    return res.send("OK")
  });

  router.get('/player/:playerId/volume/:value', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.setVolume(req.params.value)
    return res.send("OK")
  });

  router.get('/player/:playerId/track/next', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.nextTrack()
    return res.send("OK")
  });

  router.get('/player/:playerId/track/previous', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.previousTrack()
    return res.send("OK")
  });

  router.get('/player/:playerId/track/seek/:trackNo', function(req, res) {
    const player = discovery.getPlayer(req.params.playerId);
    player.trackSeek(req.params.trackNo)
    return res.send("OK")
  });

  return router;

}